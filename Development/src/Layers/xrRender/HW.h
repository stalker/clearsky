#pragma once

#include "hwcaps.h"

class  CHW
	: public pureAppActivate,
	public pureAppDeactivate
{
	//	Functions section
public:
	CHW();
	~CHW();

	void					CreateD3D();
	void					DestroyD3D();
	void					CreateDevice(HWND hw, bool move_window);

	void					DestroyDevice();

	void					Reset(HWND hw);

	void					selectResolution(u32 &dwWidth, u32 &dwHeight, BOOL bWindowed);
	D3DFORMAT				selectDepthStencil(D3DFORMAT);
	u32						selectPresentInterval();
	u32						selectGPU();
	u32						selectRefresh(u32 dwWidth, u32 dwHeight, D3DFORMAT fmt);
	void					updateWindowProps(HWND hw);
	BOOL					support(D3DFORMAT fmt, DWORD type, DWORD usage);

#ifdef DEBUG
	void	Validate(void)	{};
#else
	void	Validate(void)	{};
#endif

	//	Variables section
public:
	IDXGIAdapter*			m_pAdapter;	//	pD3D equivalent
	ID3D10Device1*       pDevice1;	//	combine with DX9 pDevice via typedef
	ID3D10Device*        pDevice;	//	combine with DX9 pDevice via typedef
	IDXGISwapChain*         m_pSwapChain;
	ID3D10RenderTargetView*	pBaseRT;	//	combine with DX9 pBaseRT via typedef
	ID3D10DepthStencilView*	pBaseZB;

	CHWCaps					Caps;

	D3D10_DRIVER_TYPE		m_DriverType;	//	DevT equivalent
	DXGI_SWAP_CHAIN_DESC	m_ChainDesc;	//	DevPP equivalent
	bool					m_bUsePerfhud;
	void			UpdateViews();
	DXGI_RATIONAL	selectRefresh(u32 dwWidth, u32 dwHeight, DXGI_FORMAT fmt);

	virtual	void	OnAppActivate();
	virtual void	OnAppDeactivate();
private:
	bool					m_move_window;
};

extern ECORE_API CHW		HW;