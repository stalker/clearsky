#pragma once

//	Interface
IC HRESULT CreateQuery(ID3DQuery **ppQuery, D3DQUERYTYPE Type);
IC HRESULT GetData(ID3DQuery *pQuery, void *pData, UINT DataSize);
IC HRESULT BeginQuery(ID3DQuery *pQuery);
IC HRESULT EndQuery(ID3DQuery *pQuery);

//	Implementation

IC HRESULT CreateQuery(ID3DQuery **ppQuery, D3DQUERYTYPE Type)
{
	D3D10_QUERY_DESC	desc;
	desc.MiscFlags = 0;

	switch (Type)
	{
	case D3DQUERYTYPE_OCCLUSION:
		desc.Query = D3D10_QUERY_OCCLUSION;
		break;
	default:
		VERIFY(!"No default.");
	}

	return HW.pDevice->CreateQuery(&desc, ppQuery);
}

IC HRESULT GetData(ID3DQuery *pQuery, void *pData, UINT DataSize)
{
	//	Use D3D10_ASYNC_GETDATA_DONOTFLUSH for prevent flushing
	return pQuery->GetData(pData, DataSize, 0);
}

IC HRESULT BeginQuery(ID3DQuery *pQuery)
{
	pQuery->Begin();
	return S_OK;
}

IC HRESULT EndQuery(ID3DQuery *pQuery)
{
	pQuery->End();
	return S_OK;
}