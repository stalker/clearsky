#include "pch_script.h"
#include "Level.h"

extern	void	GetCDKey_FromRegistry(char* CDKeyStr);
char const * ComputeClientDigest(string128& dest)
{
	return "";
}

void CLevel::SendClientDigestToServer()
{
	string128 tmp_digest;
	NET_Packet P;
	P.w_begin(M_SV_DIGEST);
	P.w_stringZ(ComputeClientDigest(tmp_digest));
	Send(P, net_flags(TRUE, TRUE, TRUE, TRUE));
}